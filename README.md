# Release

This is the latest release of EjsS (the modelling and authoring tool) in its dual Java and JavaScript 'flavours'.

After this release, which we will maintain (fixing bugs, adpating to changes in Java,...) as much as we can, we split the tool
into 'Java EJS' and 'JavaScript EJS', each supporting development in the corresponding language.

This separation makes the download smaller and simplifies both the interface and our future development.

**HOW TO INSTALL AND RUN EjsS**

Because *EjsS* requires Java 1.8 (an old but still available from Sun release of Java) and you probably don't have it in your computer, 
we have created installers that will copy both a copy of Java 8 and the EjsS-related files. It will also create a launcher executable for 
your operating system, and an uninstaller.

To install *EjsS* using the installers, download the one for your operating system and run it. The installer should work in a way familiar to you.

**CAVEAT:** You MUST have read and write permission on the destination directory where you install EjsS. Sometimes this can be tricky. For instance, MacOS Catalina 
is very strict on permissions to some folders (such as Documents, for instance). In this case, use your user home folder to 
unzip our EjsS distribution.

Running:

Inside the installation directory, you will see two folders and two executables:
- The folders are named *java* (contains a copy of Java 8, don't touch it), and *EjsS_5.33* (with all the EjsS related stuff).
- The executables are named *EjsS-launcher* and *uninstall*. Double click them to launch or uninstall EjsS, respectively.

You can also create an alias (link) to the *EjsS-launcher* anywhwere in your system (such as in the Desktop).


**EXPERT JAVA USERS:** If you know your way with Java and have or can install your own Java 1.8, you can just download the latest *EjsS_5.33_YYMMDD.zip* file.
It contains just the EJS-related files. Unzip this file and then double-click the *EjsConsole.jar* file. Make sure you have selected Java 1.8 to be your default Java plug-in (JRE) for running jar files.

- If double-clicking does not work (as in some Linux systems), do the following:    
	- Open a Terminal window
	- Change the working directory (cd) to the one that contains the unzipped *EjsConsole.jar* file
	- Type: java -jar EjsConsole.jar
	
**CAVEAT:** This should work. If it doesn't, make sure that you are using Java 8.

**Good luck and enjoy this legacy release of EjsS or move to the newer Java EJS or JavaScript EJS versions!**